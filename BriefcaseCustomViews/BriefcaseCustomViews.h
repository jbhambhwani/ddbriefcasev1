//
//  BriefcaseCustomViews.h
//  BriefcaseCustomViews
//
//  Created by Bhambhwani, Jaikumar (US - Mumbai) on 6/30/14.
//  Copyright (c) 2014 Deloitte. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BriefcaseCustomViews.
FOUNDATION_EXPORT double BriefcaseCustomViewsVersionNumber;

//! Project version string for BriefcaseCustomViews.
FOUNDATION_EXPORT const unsigned char BriefcaseCustomViewsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BriefcaseCustomViews/PublicHeader.h>


