//
//  LandingViewButton.swift
//  DD Briefcase
//
//  Created by Bhambhwani, Jaikumar (US - Mumbai) on 6/30/14.
//  Copyright (c) 2014 Deloitte. All rights reserved.
//

import UIKit
import QuartzCore

@IBDesignable
class LandingViewButton: UIButton {
    
    @IBInspectable
    var fillColor : UIColor = UIColor(red: 0.408, green: 0.737, blue: 0.271, alpha: 0.75) {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var strokeColor : UIColor = UIColor(red: 0.408, green: 0.737, blue: 0.271, alpha: 1.000) {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var strokeWidth : CGFloat = 2 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var cornerRadius : CGFloat = 30 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var textValue: String = "Show Awesomeness!" {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        self.backgroundColor = UIColor.clearColor()
        drawButtonWithFrame(rect, cornerRadius: self.cornerRadius, strokeWidth: self.strokeWidth, fillColor: self.fillColor, strokeColor: self.strokeColor, textValue: self.textValue)
    }
    
    func drawButtonWithFrame(frame: CGRect, cornerRadius: CGFloat, strokeWidth: CGFloat, fillColor: UIColor, strokeColor: UIColor, textValue: String) {
        
        //// Rectangle Drawing
        let rectanglePath = UIBezierPath(roundedRect: CGRectMake(frame.minX + 2, frame.minY + 2, frame.width - 4, frame.height - 4), cornerRadius: cornerRadius)
        fillColor.setFill()
        rectanglePath.fill()
        
        
        //// Text Drawing
        let textRect: CGRect = CGRectMake(CGFloat(frame.minX) + CGFloat(floor(Double((frame.width - 173) * 0.50000 + 0.5))), CGFloat(frame.minY) + CGFloat(floor(Double((frame.height - 16) * 0.54545 + 0.5))), 173, 16)
        let textStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as NSMutableParagraphStyle
        textStyle.alignment = NSTextAlignment.Center;
        
        let textFontAttributes = [NSFontAttributeName: UIFont(name: "FrutigerNextPro-Light", size: 18), NSForegroundColorAttributeName: UIColor.whiteColor(), NSParagraphStyleAttributeName: textStyle]
        
        NSString(string: textValue).drawInRect(textRect, withAttributes: textFontAttributes);
        
        //Mask out the remaining view
        var maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.path = rectanglePath.CGPath
        self.layer.mask = maskLayer
        
    }
    
}
