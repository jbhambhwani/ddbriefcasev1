//
//  BeaconFinderView.swift
//  DD Briefcase
//
//  Created by Bhambhwani, Jaikumar (US - Mumbai) on 6/30/14.
//  Copyright (c) 2014 Deloitte. All rights reserved.
//

import UIKit
import QuartzCore


class BeaconScanLayer: CALayer {
    
    init() {
        super.init()
        self.opaque = false;
        self.delegate = self
    }
    
    override func displayLayer(layer: CALayer!) {
        UIGraphicsBeginImageContextWithOptions(layer.bounds.size, false, 0);
        var ctx: CGContextRef = UIGraphicsGetCurrentContext()
        self.drawLayer(layer, inContext: ctx)
        var imageOfContent: UIImage = UIGraphicsGetImageFromCurrentImageContext();
        layer.contents = imageOfContent.CGImage;
        UIGraphicsEndImageContext();
        
        var rotate: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotate.toValue = CGFloat(2 * M_PI)
        rotate.duration = 2.5
        rotate.repeatCount = 1e50
        rotate.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionLinear)
        self.addAnimation(rotate, forKey:"rotateAnimation")
    }
    
    override func drawLayer(layer: CALayer!, inContext ctx: CGContext!)  {
        UIGraphicsPushContext(ctx)
        drawScanLayer(self.bounds)
        UIGraphicsPopContext()
    }
    
    func drawScanLayer(frame: CGRect) {
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()
        
        //// Color Declarations
        let color19 = UIColor(red: 0.000, green: 0.000, blue: 0.000, alpha: 0.000)
        let color23 = UIColor(red: 0.094, green: 0.157, blue: 0.071, alpha: 0.273)
        let color26 = UIColor(red: 0.399, green: 0.731, blue: 0.272, alpha: 0.961)
        let color30 = UIColor(red: 0.367, green: 0.666, blue: 0.251, alpha: 0.896)
        
        //// Gradient Declarations
        let sVGID_3_ = CGGradientCreateWithColors(CGColorSpaceCreateDeviceRGB(), [color19.CGColor, color23.CGColor, color30.CGColor, color26.CGColor], [0, 0.28, 0.93, 1])
        
        //// Bezier 3 Drawing
        var bezier3Path = UIBezierPath()
        bezier3Path.moveToPoint(CGPointMake(frame.minX + 0.50000 * frame.width, frame.minY + 0.00000 * frame.height))
        bezier3Path.addCurveToPoint(CGPointMake(frame.minX + 0.00000 * frame.width, frame.minY + 0.49412 * frame.height), controlPoint1: CGPointMake(frame.minX + 0.22386 * frame.width, frame.minY + 0.00000 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.00000 * frame.width, frame.minY + 0.22123 * frame.height))
        bezier3Path.addLineToPoint(CGPointMake(frame.minX + 1.00000 * frame.width, frame.minY + 0.49412 * frame.height))
        bezier3Path.addCurveToPoint(CGPointMake(frame.minX + 0.50000 * frame.width, frame.minY + 0.00000 * frame.height), controlPoint1: CGPointMake(frame.minX + 1.00000 * frame.width, frame.minY + 0.22123 * frame.height), controlPoint2: CGPointMake(frame.minX + 0.77614 * frame.width, frame.minY + 0.00000 * frame.height))
        bezier3Path.closePath()
        bezier3Path.miterLimit = 4;
        
        CGContextSaveGState(context)
        bezier3Path.addClip()
        let bezier3Bounds: CGRect = CGPathGetPathBoundingBox(bezier3Path.CGPath)
        CGContextDrawLinearGradient(context, sVGID_3_,
            CGPointMake(bezier3Bounds.midX + 0 * bezier3Bounds.width / 425, bezier3Bounds.midY + 105 * bezier3Bounds.height / 210),
            CGPointMake(bezier3Bounds.midX + 194.2 * bezier3Bounds.width / 425, bezier3Bounds.midY + 158.28 * bezier3Bounds.height / 210),
            UInt32(kCGGradientDrawsBeforeStartLocation) | UInt32(kCGGradientDrawsAfterEndLocation))
        CGContextRestoreGState(context)
    }
}

class BeaconFinderLayer: CAShapeLayer {
 
    var scanLayer: BeaconScanLayer
    
    init(layer: AnyObject!) {
        self.scanLayer = BeaconScanLayer()
        super.init(layer: layer)
        self.insertSublayer(self.scanLayer, atIndex:0)
    }
    
    init() {
        self.scanLayer = BeaconScanLayer()
        super.init()
        self.insertSublayer(self.scanLayer, atIndex:0)
    }
}

class BeaconFinderView: UIView {
    
    @IBInspectable
    var baseColor : UIColor = UIColor(red: 0.408, green: 0.737, blue: 0.271, alpha: 1.000) {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var firstLevelRect  : CGRect = CGRectZero
    var secondLevelRect : CGRect = CGRectZero
    var thirdLevelRect  : CGRect = CGRectZero
    
    override class func layerClass() -> AnyClass {
        return BeaconFinderLayer.self
    }
    
    override func layoutSubviews() {
        self.backgroundColor = UIColor.clearColor()
        self.layer.opaque = false
        self.layer.setNeedsDisplay()
        (self.layer as BeaconFinderLayer).scanLayer.frame = self.layer.bounds
        (self.layer as BeaconFinderLayer).scanLayer.setNeedsDisplay()
        (self.layer as BeaconFinderLayer).scanLayer.opacity = 0.6
    }
    
//    override func drawRect(rect: CGRect) {
//        drawFinderViewWithFrame(rect, baseColor: self.baseColor)
//    }
    
    override func drawLayer(layer: CALayer!, inContext ctx: CGContext!) {
        UIGraphicsPushContext(ctx);
        drawFinderViewWithFrame(self.bounds, baseColor: self.baseColor)
        UIGraphicsPopContext()
    }
    
//    override func layoutSublayersOfLayer(layer: CALayer!) {
//        var baseLayer = self.layer as BeaconFinderLayer
//        baseLayer.scanLayer.frame = self.bounds
//        baseLayer.scanLayer.setNeedsDisplay()
//    }
    
    func drawFinderViewWithFrame(frame: CGRect, baseColor: UIColor) {
        
        //// Color Declarations
        let baseColor = UIColor(red: 0.408, green: 0.737, blue: 0.271, alpha: 1.000)
        let firstLevelShade = baseColor.colorWithAlphaComponent(0.6)
        let secondLevelShade = baseColor.colorWithAlphaComponent(0.4)
        let thirdLevelShade = baseColor.colorWithAlphaComponent(0.2)
        let iPhoneColor = UIColor(red: 0.005, green: 0.005, blue: 0.005, alpha: 1.000)
        
        
        //// Subframes
        let group: CGRect = CGRectMake(frame.minX, frame.minY + 1, frame.width - 1, frame.height - 1)
        let iPhoneIcon: CGRect = CGRectMake(CGFloat(frame.minX) + CGFloat(floor(Double((frame.width - 27.07) * 0.49884 + 0.47) + 0.03)), CGFloat(frame.minY) + CGFloat(floor(Double((frame.height - 53.14) * 0.49712 + 0.07))) + 0.43, 27.07, 53.14)
        
        
        //// Group
        //// thirdLevelCircle Drawing
        self.thirdLevelRect = CGRectMake(CGFloat(group.minX) + CGFloat(floor(Double(group.width * 0.00000 + 0.5))), group.minY + CGFloat(floor(Double(group.height * 0.00000 + 0.5))), CGFloat(floor(Double(group.width * 1.00000 + 0.5))) - CGFloat(floor(Double(group.width * 0.00000 + 0.5))), CGFloat(floor(Double(group.height * 1.00000 + 0.5))) - CGFloat(floor(Double(group.height * 0.00000 + 0.5))))
        var thirdLevelCirclePath = UIBezierPath(ovalInRect: self.thirdLevelRect)
        thirdLevelShade.setFill()
        thirdLevelCirclePath.fill()

        
        
        //// secondLevel Drawing
        self.secondLevelRect = CGRectMake(CGFloat(group.minX) + CGFloat(floor(Double(group.width * 0.16541 + 0.5))), CGFloat(group.minY) + CGFloat(floor(Double(group.height * 0.16291 + 0.5))), CGFloat(floor(Double(group.width * 0.83709 + 0.5))) - CGFloat(floor(Double(group.width * 0.16541 + 0.5))), CGFloat(floor(Double(group.height * 0.83459 + 0.5))) - CGFloat(floor(Double(group.height * 0.16291 + 0.5))))
        var secondLevelPath = UIBezierPath(ovalInRect: self.secondLevelRect)
        secondLevelShade.setFill()
        secondLevelPath.fill()
        
        
        //// firstLevelCircle Drawing
        self.firstLevelRect = CGRectMake(CGFloat(group.minX) + CGFloat(floor(Double(group.width * 0.33333 + 0.5))), CGFloat(group.minY) + CGFloat(floor(Double(group.height * 0.32832 + 0.5))), CGFloat(floor(Double(group.width * 0.67168 + 0.5))) - CGFloat(floor(Double(group.width * 0.33333 + 0.5))), CGFloat(floor(Double(group.height * 0.66667 + 0.5))) - CGFloat(floor(Double(group.height * 0.32832 + 0.5))))
        var firstLevelCirclePath = UIBezierPath(ovalInRect: self.firstLevelRect)
        firstLevelShade.setFill()
        firstLevelCirclePath.fill()
        
        
        
        
        //// iPhoneIcon
        //// Bezier Drawing
        var bezierPath = UIBezierPath()
        bezierPath.moveToPoint(CGPointMake(iPhoneIcon.minX + 22.91, iPhoneIcon.minY))
        bezierPath.addLineToPoint(CGPointMake(iPhoneIcon.minX + 4.16, iPhoneIcon.minY))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX, iPhoneIcon.minY + 4.16), controlPoint1: CGPointMake(iPhoneIcon.minX + 1.87, iPhoneIcon.minY), controlPoint2: CGPointMake(iPhoneIcon.minX, iPhoneIcon.minY + 1.86))
        bezierPath.addLineToPoint(CGPointMake(iPhoneIcon.minX, iPhoneIcon.minY + 48.98))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 4.16, iPhoneIcon.minY + 53.14), controlPoint1: CGPointMake(iPhoneIcon.minX, iPhoneIcon.minY + 51.28), controlPoint2: CGPointMake(iPhoneIcon.minX + 1.87, iPhoneIcon.minY + 53.14))
        bezierPath.addLineToPoint(CGPointMake(iPhoneIcon.minX + 22.91, iPhoneIcon.minY + 53.14))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 27.07, iPhoneIcon.minY + 48.98), controlPoint1: CGPointMake(iPhoneIcon.minX + 25.21, iPhoneIcon.minY + 53.14), controlPoint2: CGPointMake(iPhoneIcon.minX + 27.07, iPhoneIcon.minY + 51.28))
        bezierPath.addLineToPoint(CGPointMake(iPhoneIcon.minX + 27.07, iPhoneIcon.minY + 4.16))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 22.91, iPhoneIcon.minY), controlPoint1: CGPointMake(iPhoneIcon.minX + 27.07, iPhoneIcon.minY + 1.86), controlPoint2: CGPointMake(iPhoneIcon.minX + 25.21, iPhoneIcon.minY))
        bezierPath.closePath()
        bezierPath.moveToPoint(CGPointMake(iPhoneIcon.minX + 11.49, iPhoneIcon.minY + 4.42))
        bezierPath.addLineToPoint(CGPointMake(iPhoneIcon.minX + 15.58, iPhoneIcon.minY + 4.42))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 16.14, iPhoneIcon.minY + 4.97), controlPoint1: CGPointMake(iPhoneIcon.minX + 15.89, iPhoneIcon.minY + 4.42), controlPoint2: CGPointMake(iPhoneIcon.minX + 16.14, iPhoneIcon.minY + 4.67))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 15.58, iPhoneIcon.minY + 5.53), controlPoint1: CGPointMake(iPhoneIcon.minX + 16.14, iPhoneIcon.minY + 5.28), controlPoint2: CGPointMake(iPhoneIcon.minX + 15.89, iPhoneIcon.minY + 5.53))
        bezierPath.addLineToPoint(CGPointMake(iPhoneIcon.minX + 11.49, iPhoneIcon.minY + 5.53))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 10.93, iPhoneIcon.minY + 4.97), controlPoint1: CGPointMake(iPhoneIcon.minX + 11.18, iPhoneIcon.minY + 5.53), controlPoint2: CGPointMake(iPhoneIcon.minX + 10.93, iPhoneIcon.minY + 5.28))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 11.49, iPhoneIcon.minY + 4.42), controlPoint1: CGPointMake(iPhoneIcon.minX + 10.93, iPhoneIcon.minY + 4.67), controlPoint2: CGPointMake(iPhoneIcon.minX + 11.18, iPhoneIcon.minY + 4.42))
        bezierPath.closePath()
        bezierPath.moveToPoint(CGPointMake(iPhoneIcon.minX + 9.1, iPhoneIcon.minY + 4.21))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 9.76, iPhoneIcon.minY + 4.87), controlPoint1: CGPointMake(iPhoneIcon.minX + 9.47, iPhoneIcon.minY + 4.21), controlPoint2: CGPointMake(iPhoneIcon.minX + 9.76, iPhoneIcon.minY + 4.5))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 9.1, iPhoneIcon.minY + 5.53), controlPoint1: CGPointMake(iPhoneIcon.minX + 9.76, iPhoneIcon.minY + 5.23), controlPoint2: CGPointMake(iPhoneIcon.minX + 9.47, iPhoneIcon.minY + 5.53))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 8.44, iPhoneIcon.minY + 4.87), controlPoint1: CGPointMake(iPhoneIcon.minX + 8.74, iPhoneIcon.minY + 5.53), controlPoint2: CGPointMake(iPhoneIcon.minX + 8.44, iPhoneIcon.minY + 5.23))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 9.1, iPhoneIcon.minY + 4.21), controlPoint1: CGPointMake(iPhoneIcon.minX + 8.44, iPhoneIcon.minY + 4.5), controlPoint2: CGPointMake(iPhoneIcon.minX + 8.74, iPhoneIcon.minY + 4.21))
        bezierPath.closePath()
        bezierPath.moveToPoint(CGPointMake(iPhoneIcon.minX + 13.53, iPhoneIcon.minY + 50.9))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 10.93, iPhoneIcon.minY + 48.29), controlPoint1: CGPointMake(iPhoneIcon.minX + 12.1, iPhoneIcon.minY + 50.9), controlPoint2: CGPointMake(iPhoneIcon.minX + 10.93, iPhoneIcon.minY + 49.73))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 13.53, iPhoneIcon.minY + 45.69), controlPoint1: CGPointMake(iPhoneIcon.minX + 10.93, iPhoneIcon.minY + 46.86), controlPoint2: CGPointMake(iPhoneIcon.minX + 12.1, iPhoneIcon.minY + 45.69))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 16.14, iPhoneIcon.minY + 48.29), controlPoint1: CGPointMake(iPhoneIcon.minX + 14.97, iPhoneIcon.minY + 45.69), controlPoint2: CGPointMake(iPhoneIcon.minX + 16.14, iPhoneIcon.minY + 46.86))
        bezierPath.addCurveToPoint(CGPointMake(iPhoneIcon.minX + 13.53, iPhoneIcon.minY + 50.9), controlPoint1: CGPointMake(iPhoneIcon.minX + 16.14, iPhoneIcon.minY + 49.73), controlPoint2: CGPointMake(iPhoneIcon.minX + 14.97, iPhoneIcon.minY + 50.9))
        bezierPath.closePath()
        bezierPath.moveToPoint(CGPointMake(iPhoneIcon.minX + 24.99, iPhoneIcon.minY + 43.84))
        bezierPath.addLineToPoint(CGPointMake(iPhoneIcon.minX + 2.08, iPhoneIcon.minY + 43.84))
        bezierPath.addLineToPoint(CGPointMake(iPhoneIcon.minX + 2.08, iPhoneIcon.minY + 9.3))
        bezierPath.addLineToPoint(CGPointMake(iPhoneIcon.minX + 24.99, iPhoneIcon.minY + 9.3))
        bezierPath.addLineToPoint(CGPointMake(iPhoneIcon.minX + 24.99, iPhoneIcon.minY + 43.84))
        bezierPath.closePath()
        bezierPath.miterLimit = 4;
        
        iPhoneColor.setFill()
        bezierPath.fill()
    }
    
}
