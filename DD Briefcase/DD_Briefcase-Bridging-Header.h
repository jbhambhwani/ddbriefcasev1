//
//  DD_Briefcase.h
//  DD Briefcase
//
//  Created by Bhambhwani, Jaikumar (US - Mumbai) on 6/26/14.
//  Copyright (c) 2014 Deloitte. All rights reserved.
//

#import <Restkit/Restkit.h>
#import <AFNetworking/AFNetworking.h>
#import "RKManagedObjectRequestOperation.h"
#import "BriefcaseIcons.h"
