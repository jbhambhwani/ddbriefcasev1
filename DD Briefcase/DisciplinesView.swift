//
//  DisciplinesView.swift
//  DD Briefcase
//
//  Created by Bhambhwani, Jaikumar (US - Mumbai) on 7/3/14.
//  Copyright (c) 2014 Deloitte. All rights reserved.
//

import UIKit

extension UIImageView {
    
    convenience init(frame: CGRect, discipline: String, color: UIColor) {
        
        self.init(frame: frame)
        var image: UIImage?
        switch discipline {
            case "creative":
                BriefcaseIcons.setColorForCreativeIcon(color)
                image = BriefcaseIcons.imageOfCreativeIconWithContainer(self.bounds)
            case "ux":
                BriefcaseIcons.setColorForUXIcon(color)
                image = BriefcaseIcons.imageOfUXIconWithContainer(self.bounds)
            case "ios":
                BriefcaseIcons.setColorForAppleIcon(color)
                image = BriefcaseIcons.imageOfAppleIconWithContainer(self.bounds)
            case "android":
                BriefcaseIcons.setColorForAndroidIcon(color)
                image = BriefcaseIcons.imageOfAndroiIconWithContainer(self.bounds)
            case "frontEnd":
                BriefcaseIcons.setColorForFEIcon(color)
                image = BriefcaseIcons.imageOfFEIconWithFrame(self.bounds)
            case "qa":
                BriefcaseIcons.setColorForQAIcon(color)
                image = BriefcaseIcons.imageOfQAIconWithContainer(self.bounds)
            default:
                image = nil
        }
        self.image = image
    }
}

class DisciplinesView: UIView {

    var separationDistance: CGFloat = 10.0
    
    var disciplineIconcolor: UIColor = BriefcaseIcons.colorForIcon() {
        didSet {
            self.addDisciplineImageViews()
        }
    }
    
    var cellSize: CGFloat = 25.0
    
    var disciplines: Array<String> = ["qa", "creative", "ios"] {
        didSet {
            self.addDisciplineImageViews()
        }
    }
    
    init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    init(coder aDecoder: NSCoder!) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        self.addDisciplineImageViews()
    }
    
    func addDisciplineImageViews() {
        self.backgroundColor = UIColor.clearColor()
        for view : AnyObject in self.subviews {
            view.removeFromSuperview()
        }
        for index in 0..disciplines.count  {
            var imageFrame = CGRectMake(CGFloat(index) * (cellSize + separationDistance), 0.0, cellSize, cellSize)
            var imageView: UIImageView = UIImageView(frame: imageFrame, discipline:disciplines[index], color: self.disciplineIconcolor)
            self.addSubview(imageView)
        }
    }
}
