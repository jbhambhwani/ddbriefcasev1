//
//  LandingViewController.swift
//  DD Briefcase
//
//  Created by Bhambhwani, Jaikumar (US - Mumbai) on 6/30/14.
//  Copyright (c) 2014 Deloitte. All rights reserved.
//

import UIKit
import CoreText
import MediaPlayer
import BriefcaseCustomViews

class LandingViewController: UIViewController {

    @IBOutlet var landingTitle: UILabel
    
    @IBOutlet var landingViewbutton: LandingViewButton
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addVideoPlayer()
        formatLandingLabel()
        
        landingTitle.addStandardMotionEffect(20)
        landingViewbutton.addStandardMotionEffect(20)
        
    }
    
    func formatLandingLabel() {
        
        let textStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as NSMutableParagraphStyle
        textStyle.alignment = NSTextAlignment.Left;
        var attributes: Dictionary = [NSFontAttributeName: UIFont(name: "FrutigerNextPro-Light", size: 32), NSForegroundColorAttributeName: UIColor.whiteColor(), NSParagraphStyleAttributeName: textStyle]
        
        var indexOfDot: Int = landingTitle.text.bridgeToObjectiveC().rangeOfString(".").location
        var landingTitleText: NSMutableAttributedString = NSMutableAttributedString(string: landingTitle.text.substringToIndex(indexOfDot), attributes: attributes)
        
        attributes[NSFontAttributeName] = UIFont(name: "Avenir-Heavy", size: 4)
        landingTitleText.appendAttributedString(NSAttributedString(string:" ", attributes: attributes))
        attributes[NSForegroundColorAttributeName] = UIColor(red: 0.408, green: 0.737, blue: 0.271, alpha: 1)
        
        attributes[NSFontAttributeName] = UIFont(name: "Avenir-Heavy", size: 20)
        landingTitleText.appendAttributedString(NSAttributedString(string:".", attributes: attributes))
        
        attributes[NSFontAttributeName] = UIFont(name: "FrutigerNextPro-Light", size: 32)
        attributes[NSForegroundColorAttributeName] = UIColor.whiteColor()
        landingTitleText.appendAttributedString(NSAttributedString(string:landingTitle.text.substringFromIndex(indexOfDot + 1), attributes: attributes))
        
        landingTitle.attributedText = landingTitleText
        
    }
    
    func addVideoPlayer() {
        
        var videopath: String = NSBundle.mainBundle().pathForResource("video-01", ofType: "mp4")
        var videoURL: NSURL = NSURL(fileURLWithPath: videopath)
        
        var moviePlayerVC: MPMoviePlayerViewController = MPMoviePlayerViewController(contentURL: videoURL)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("moviePlaybackComplete:"), name:MPMoviePlayerLoadStateDidChangeNotification , object: moviePlayerVC.moviePlayer)
        
        moviePlayerVC.moviePlayer.repeatMode = .One
        moviePlayerVC.moviePlayer.controlStyle = .None
        
        
        moviePlayerVC.willMoveToParentViewController(self)
        self.addChildViewController(moviePlayerVC)
        self.view.addSubview(moviePlayerVC.view)
        self.view.sendSubviewToBack(moviePlayerVC.view)
        moviePlayerVC.didMoveToParentViewController(self)
        
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showBeaconFinder(sender: AnyObject) {
        self.performSegueWithIdentifier(
            "showBeaconFinderSegue", sender: self)
    }

    func moviePlaybackComplete(notification: NSNotification) {
        
    }
    
    /*
    // #pragma mark - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
