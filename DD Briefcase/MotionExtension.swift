//
//  MotionExtension.swift
//  DD Briefcase
//
//  Created by Bhambhwani, Jaikumar (US - Mumbai) on 7/10/14.
//  Copyright (c) 2014 Deloitte. All rights reserved.
//

import UIKit

extension UIView {

    func addStandardMotionEffect(intensity: CGFloat) {
        
        // Set vertical effect
        var verticalMotionEffect: UIInterpolatingMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: UIInterpolatingMotionEffectType.TiltAlongHorizontalAxis)
        
        verticalMotionEffect.minimumRelativeValue = -intensity
        verticalMotionEffect.maximumRelativeValue = intensity
        
        // Set horizontal effect
        var horizontalMotionEffect: UIInterpolatingMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: UIInterpolatingMotionEffectType.TiltAlongVerticalAxis)
        
        horizontalMotionEffect.minimumRelativeValue = -intensity
        horizontalMotionEffect.maximumRelativeValue = intensity
        
        // Create group to combine both
        var group: UIMotionEffectGroup = UIMotionEffectGroup()
        group.motionEffects = [horizontalMotionEffect, verticalMotionEffect];
        
        // Add both effects to your view
        self.addMotionEffect(group)
    }

}
