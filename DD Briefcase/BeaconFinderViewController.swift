//
//  BeaconFinderViewController.swift
//  DD Briefcase
//
//  Created by Bhambhwani, Jaikumar (US - Mumbai) on 6/30/14.
//  Copyright (c) 2014 Deloitte. All rights reserved.
//

import UIKit

class BeaconFinderViewController: UIViewController {

    @IBOutlet var listButton: UIButton
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    listButton.setImage(BriefcaseIcons.imageOfListIconWithContainer(self.listButton.bounds), forState: .Normal)
        
    }

    /*
    // #pragma mark - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
