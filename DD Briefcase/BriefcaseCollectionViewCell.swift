//
//  BriefcaseCollectionViewCell.swift
//  DD Briefcase
//
//  Created by Bhambhwani, Jaikumar (US - Mumbai) on 7/3/14.
//  Copyright (c) 2014 Deloitte. All rights reserved.
//

import UIKit

class BriefcaseCollectionViewCell: UICollectionViewCell {

    init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    init(coder aDecoder: NSCoder!) {
        super.init(coder: aDecoder)
    }
    
}
